* textFeatures.py
    Contains methods for converting a corpus to a dictionary, and documents to vectors.
* bayes.py
    Contains features for training/classifying a Naive Bayes Classifier
* main.py 
    Calls the appropriate methods and displays verbose output based on input
* hw2.py
    Contains code to reproduce the experiments described in the writeup
* proc-output.py
    Processes all output files from hw2.py and displays them in the console
* writeup.pdf
    Contains the writeup from the homework description 
* data/
    Contains input data
* output/
    Where output files from hw2.py live
